/*
 * Jetfinder.h
 *
 *  Created on: Feb 10, 2016
 *      Author: markus
 */

#ifndef JETFINDER_H_
#define JETFINDER_H_

#include <TObject.h>
#include <TObjArray.h>
#include <fastjet/JetDefinition.hh>

class TLorentzVector;
class TParticle;

class Jetfinder : public TObject {
public:
	Jetfinder(double radius = 0.4);
	virtual ~Jetfinder() {}

	void Reset();
	void FindJets();

	void AddParticle(Double_t px, Double_t py, Double_t pz, Double_t e);
	void AddParticle(const TLorentzVector &part);
	void AddParticle(const TParticle &part);

	const TObjArray &GetReconstructedJets() const { return fReconstructedJets; }

private:
	TObjArray 								fInputParticles;
	TObjArray 								fReconstructedJets;
	fastjet::JetDefinition					fJetDefinition;

	ClassDef(Jetfinder, 1);
};

#endif /* JETFINDER_H_ */
