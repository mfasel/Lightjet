/*
 * Recjet.h
 *
 *  Created on: Feb 10, 2016
 *      Author: markus
 */

#ifndef RECJET_H_
#define RECJET_H_

#include <TObject.h>
#include <TObjArray.h>
#include <TLorentzVector.h>

class Recjet : public TObject {
public:
	Recjet();
	Recjet(double px, double py, double pz, double e);
	Recjet(const TLorentzVector &ref);
	virtual ~Recjet() {}

	void AddJetConstituent(double px, double py, double pz, double e) { fJetConstituents.Add(new TLorentzVector(px, py, pz, e)); }
	void AddJetConstituent(const TLorentzVector &vec) { fJetConstituents.Add(new TLorentzVector(vec)); }
	void SetJetVector(const TLorentzVector &ref) { fJetVector = ref; }
	void SetJetVector(double px, double py, double pz, double e) { fJetVector.SetPxPyPzE(px, py, pz, e); }

	const TObjArray &GetListOfConstituents() const { return fJetConstituents; }
	const TLorentzVector &GetJetVector() const { return fJetVector; }

	TObjArray GetConstituentsAfterPtCut(double ptcut) const;

private:
	TLorentzVector				fJetVector;
	TObjArray					fJetConstituents;

	ClassDef(Recjet, 1);
};

#endif /* RECJET_H_ */
