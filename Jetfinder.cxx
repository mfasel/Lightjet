/*
 * Jetfinder.cxx
 *
 *  Created on: Feb 10, 2016
 *      Author: markus
 */

#include "Jetfinder.h"
#include "Recjet.h"

#include <vector>

#include <TLorentzVector.h>
#include <TParticle.h>

#include <fastjet/PseudoJet.hh>
#include <fastjet/ClusterSequence.hh>

ClassImp(Jetfinder)

Jetfinder::Jetfinder(double radius) :
	TObject(),
	fInputParticles(),
	fReconstructedJets(),
	fJetDefinition(fastjet::antikt_algorithm, radius)
{
	fInputParticles.SetOwner(true);
	fReconstructedJets.SetOwner(true);
}

void Jetfinder::Reset(){
	fInputParticles.Clear();
	fReconstructedJets.Clear();
}

void Jetfinder::AddParticle(double px, double py, double pz, double e) {
	fInputParticles.Add(new TLorentzVector(px, py, pz, e));
}

void Jetfinder::AddParticle(const TLorentzVector &part) {
	fInputParticles.Add(new TLorentzVector(part));
}

void Jetfinder::AddParticle(const TParticle &part) {
	fInputParticles.Add(new TLorentzVector(part.Px(), part.Py(), part.Pz(), part.Energy()));
}

void Jetfinder::FindJets() {
	std::vector<fastjet::PseudoJet> pseudojets;
	for(TIter trackIter = TIter(&fInputParticles).Begin(); trackIter != TIter::End(); ++trackIter){
		TLorentzVector *partvec = static_cast<TLorentzVector *>(*trackIter);
		pseudojets.push_back(fastjet::PseudoJet(partvec->Px(), partvec->Py(), partvec->Pz(), partvec->E()));
	}
	fastjet::ClusterSequence jetfinder(pseudojets, fJetDefinition);
	std::vector<fastjet::PseudoJet> recjets = sorted_by_pt(jetfinder.inclusive_jets());

	for(std::vector<fastjet::PseudoJet>::iterator jetiter = recjets.begin(); jetiter != recjets.end(); ++jetiter){
		Recjet *foundjet = new Recjet(jetiter->px(), jetiter->py(), jetiter->pz(), jetiter->E());
		fReconstructedJets.Add(foundjet);
		std::vector<fastjet::PseudoJet> constituents = jetiter->constituents();
		for(std::vector<fastjet::PseudoJet>::iterator constiter = constituents.begin(); constiter != constituents.end(); ++constiter){
			foundjet->AddJetConstituent(constiter->px(), constiter->py(), constiter->pz(), constiter->E());
		}
	}
}
