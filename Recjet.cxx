/*
 * Recjet.cxx
 *
 *  Created on: Feb 10, 2016
 *      Author: markus
 */

#include "Recjet.h"

ClassImp(Recjet);

Recjet::Recjet() :
	TObject(),
	fJetVector(),
	fJetConstituents()
{
	fJetConstituents.SetOwner(true);
}

Recjet::Recjet(double px, double py, double pz, double e) :
	TObject(),
	fJetVector(px, py, px, e),
	fJetConstituents()
{
	fJetConstituents.SetOwner(true);
}

Recjet::Recjet(const TLorentzVector &ref) :
	TObject(),
	fJetVector(ref),
	fJetConstituents()
{
	fJetConstituents.SetOwner(true);
}

TObjArray Recjet::GetConstituentsAfterPtCut(double ptcut) const {
	TObjArray result;
	result.SetOwner(false);
	for(TIter jetiter = TIter(&fJetConstituents).Begin(); jetiter != TIter::End(); ++jetiter){
		TLorentzVector *partvec = static_cast<TLorentzVector *>(*jetiter);
		if(TMath::Abs(partvec->Pt()) > ptcut) result.Add(partvec);
	}
	return result;
}
